# mkamner/containers/builder

Custom builder image based on `podman:stable` with some additional packages.

## Tags

- `latest`

## Usage

```bash
podman run -it --rm registry.gitlab.com/mkamner/containers/builder:latest
```

## Registry Availability

This image is available on the GitLab registry as `registry.gitlab.com/mkamner/containers/builder`
and on DockerHub as `docker.io/mkamner/builder`.