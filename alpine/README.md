# mkamner/containers/alpine

Non-root alpine images with a basic app user.

## Tags

- `latest`: Latest supported Alpine version
- `3.20`: Alpine 3.20
- `3.19`: Alpine 3.19

## Usage

```Dockerfile
FROM registry.gitlab.com/mkamner/containers/alpine:latest

ADD the-app/ .

CMD ["bash", "the-app.sh"]
```

## Registry Availability

This image is available on the GitLab registry as `registry.gitlab.com/mkamner/containers/alpine`
and on DockerHub as `docker.io/mkamner/alpine`.