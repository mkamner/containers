#!/bin/bash
# Get the last RELEAS_BACKSEARCH number of releases from the upstream
# and build the missing images

set -e

RELEAS_BACKSEARCH=3

REGISTRY_REPOSITORY_ID=$(curl --silent https://gitlab.com/api/v4/projects/mkamner%2Fcontainers/registry/repositories | jq -r '.[] | select(.path == "mkamner/containers/terragrunt-atlantis-config") .id')

LATEST_VERSIONS_FROM_UPSTREAM=$(curl --silent https://api.github.com/repos/transcend-io/terragrunt-atlantis-config/releases | jq -r ".[].name" | head -n $RELEAS_BACKSEARCH)

ALREADY_BUILT_VERSIONS=$(curl --silent "https://gitlab.com/api/v4/projects/mkamner%2Fcontainers/registry/repositories/$REGISTRY_REPOSITORY_ID/tags" | jq -r ".[].name")

export BUILD_DIR="terragrunt-atlantis-config/upstream/"
export IMAGE_PATH="mkamner/containers/terragrunt-atlantis-config"

for version_tag in $LATEST_VERSIONS_FROM_UPSTREAM; do
  if [[ $ALREADY_BUILT_VERSIONS == *"$version_tag"* ]]; then
    echo "Already built $version_tag"
  else
    echo "Building $version_tag"
    curl -L https://github.com/transcend-io/terragrunt-atlantis-config/archive/refs/tags/$version_tag.tar.gz | tar zx
    mv terragrunt-atlantis-config-*/ terragrunt-atlantis-config/upstream/
    # Rewrite image to FQDN
    sed -i 's|FROM golang|FROM docker.io/library/golang|g' terragrunt-atlantis-config/upstream/Dockerfile
    export IMAGE_TAG=$version_tag
    bash build.sh
    rm -rf terragrunt-atlantis-config/upstream
  fi
done
