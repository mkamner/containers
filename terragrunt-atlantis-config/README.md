# mkamner/containers/terragrunt-atlantis-config

Builds container images for [terragrunt-atlantis-config](https://github.com/transcend-io/terragrunt-atlantis-config).

## Tags

We mirror all releases starting from `v1.17.4`, automatically building new versions daily.

## Usage

As a base image:

```Dockerfile
FROM registry.gitlab.com/mkamner/containers/terragrunt-atlantis-config:v1.19.0
```

As the source of the terragrunt-atlantis-config binary:

```Dockerfile
FROM somewhere

COPY --from=registry.gitlab.com/mkamner/containers/terragrunt-atlantis-config:v1.19.0 /app/terragrunt-atlantis-config /app/terragrunt-atlantis-config

```

## Registry Availability

This image is available on the GitLab registry as `registry.gitlab.com/mkamner/containers/terragrunt-atlantis-config`
and on DockerHub as `docker.io/mkamner/terragrunt-atlantis-config`.
