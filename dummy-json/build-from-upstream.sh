#!/bin/bash
set -e

export BUILD_DIR="dummy-json/upstream/"
export IMAGE_PATH="mkamner/containers/dummy-json"
export IMAGE_TAG="latest"

REGISTRY_REPOSITORY_ID=$(curl --silent https://gitlab.com/api/v4/projects/mkamner%2Fcontainers/registry/repositories | jq -r '.[] | select(.path == "mkamner/containers/terragrunt-atlantis-config") .id')

# Figure out the last built epoch, defaulting it to 0 if it was never built yet
last_built_timestamp=$(curl --silent "https://gitlab.com/api/v4/projects/mkamner%2Fcontainers/registry/repositories/$REGISTRY_REPOSITORY_ID/tags/$IMAGE_TAG" | jq .created_at)
last_built_date=$(echo "$last_built_timestamp" | cut -d'T' -f1)
if [ "$last_built_date" == "null" ]; then
    last_built_epoch=0
else
    last_built_epoch=$(date -d "$last_built_date" +%s)
fi

# Figure out the last upstream commit epoch
last_upstream_commit_timestamp=$(curl --silent https://api.github.com/repos/ovi/dummyjson/branches/master | jq -r .commit.commit.author.date)
last_upstream_commit_date=$(echo "$last_upstream_commit_timestamp" | cut -d'T' -f1)
last_upstream_commit_epoch=$(date -d "$last_upstream_commit_date" +%s)


if (( last_upstream_commit_epoch > last_built_epoch )); then
  echo "Building for new commits"
  curl -L https://github.com/Ovi/DummyJSON/archive/refs/heads/master.tar.gz | tar zx
  mv DummyJSON-master/ $BUILD_DIR
  bash build.sh
  rm -rf $BUILD_DIR
else
  echo "Already up to date."
fi
