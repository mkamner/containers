# mkamner/containers/dummy-json

Builds container images for [DummyJSON](https://github.com/Ovi/DummyJSON).

## Tags

Since the upstream does not provide releases we mirror it's latest state as `latest`.

## Usage

```bash
podman run -it --rm --publish 8080:80 registry.gitlab.com/mkamner/containers/dummy-json:latest
```

## Registry Availability

This image is available on the GitLab registry as `registry.gitlab.com/mkamner/containers/dummy-json`
and on DockerHub as `docker.io/mkamner/dummy-json`.
