# mkamner/containers/python

Non-root python images.

## Tags

- `latest`: Latest supported Python version
- `3.12`: Python 3.12
- `3.10`: Python 3.10

## Usage

```Dockerfile
FROM registry.gitlab.com/mkamner/containers/python:latest

ADD the-app/ .

RUN pip install --user -r requirements.txt

CMD ["python", "the_app"]
```

## Registry Availability

This image is available on the GitLab registry as `registry.gitlab.com/mkamner/containers/python`
and on DockerHub as `docker.io/mkamner/python`.
