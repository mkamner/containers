# Containers

Pre-built container images for common use cases.

- [Alpine (root-less)](alpine/README.md)
- [Python (root-less)](python/README.md)
- [terragrunt-atlantis-config](terragrunt-atlantis-config/README.md)
- [dummy-json](dummy-json/README.md)
- [Builder](builder/README.md)

## About

This is a collection of opinionated base images and builds for projects with no official container image builds.

I maintain these images on a best-effort basis in my free time and appreciate any [coffee](https://buymeacoffee.com/mkamner) you might want to give to me.

If you depend on these images in your company consider my [professional support and consulting services](https://marco.ninja/#consulting).
