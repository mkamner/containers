#!/bin/bash
# Build a image to the GitLab registry

set -e

# GitLab Registry
if [ -z "$CI_REGISTRY" ]; then
    echo "Must set CI_REGISTRY"
    exit 1
fi

if [ -z "$CI_REGISTRY_USER" ]; then
    echo "Must set CI_REGISTRY_USER"
    exit 1
fi

if [ -z "$CI_REGISTRY_PASSWORD" ]; then
    echo "Must set CI_REGISTRY_PASSWORD"
    exit 1
fi

# DockerHub
if [ -z "$DOCKERHUB_REGISTRY" ]; then
    echo "Must set DOCKERHUB_REGISTRY"
    exit 1
fi

if [ -z "$DOCKERHUB_REGISTRY_USER" ]; then
    echo "Must set DOCKERHUB_REGISTRY_USER"
    exit 1
fi

if [ -z "$DOCKERHUB_REGISTRY_PASSWORD" ]; then
    echo "Must set DOCKERHUB_REGISTRY_PASSWORD"
    exit 1
fi

# Image information
if [ -z "$IMAGE_PATH" ]; then
    echo "Must set IMAGE_PATH"
    exit 1
fi

if [ -z "$IMAGE_TAG" ]; then
    echo "Must set IMAGE_TAG"
    exit 1
fi

if [ -z "$BUILD_DIR" ]; then
    echo "Must set BUILD_DIR"
    exit 1
fi

if [[ $IMAGE_PATH != mkamner/* ]]; then
  echo "IMAGE_PATH must start with mkamner/"
  exit 1
fi

TARGET_IMAGE="$CI_REGISTRY/$IMAGE_PATH:$IMAGE_TAG"
TARGET_IMAGE_LATEST="$CI_REGISTRY/$IMAGE_PATH:latest"

DOCKERHUB_IMAGE_PATH=$(echo $IMAGE_PATH | sed 's/containers\///')
DOCKERHUB_TARGET_IMAGE="$DOCKERHUB_REGISTRY/$DOCKERHUB_IMAGE_PATH:$IMAGE_TAG"
DOCKERHUB_TARGET_IMAGE_LATEST="$DOCKERHUB_REGISTRY/$DOCKERHUB_IMAGE_PATH:latest"


podman login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
podman login -u "$DOCKERHUB_REGISTRY_USER" -p "$DOCKERHUB_REGISTRY_PASSWORD" "$DOCKERHUB_REGISTRY"

echo "Building from $BUILD_DIR to $TARGET_IMAGE"
podman build -t $TARGET_IMAGE $BUILD_DIR --build-arg "BUILD_DATE=$CI_JOB_STARTED_AT" --build-arg "BUILD_REF=$CI_COMMIT_SHA"
podman tag $TARGET_IMAGE $DOCKERHUB_TARGET_IMAGE

echo "Pushing $IMAGE_TAG to registries"
podman push $TARGET_IMAGE
podman push $DOCKERHUB_TARGET_IMAGE

if [ -n "$TAG_LATEST" ]; then
  echo "Tagging to latest"
  podman tag $TARGET_IMAGE $TARGET_IMAGE_LATEST
  podman tag $DOCKERHUB_TARGET_IMAGE $DOCKERHUB_TARGET_IMAGE_LATEST

  echo "Pushing latest to registries"
  podman push $TARGET_IMAGE_LATEST
  podman push $DOCKERHUB_TARGET_IMAGE_LATEST
fi